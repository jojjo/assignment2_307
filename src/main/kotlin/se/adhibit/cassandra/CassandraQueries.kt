package se.adhibit.cassandra

import com.datastax.oss.driver.api.core.CqlSession

class CassandraQueries {
    init {
        CqlSession.builder().build().use { session ->
            // Insert data into student table
            val query = "INSERT INTO student(student_id, student_lastname, student_firstname, uni_name, student_email, program_name, department_name)" +
                    "  VALUES (1, 'Velander','Johanna', 'Linneaus University', 'johanna@student.se', 'Webtechnology', 'Computing Science');"
            val insertResult = session.execute(query)

            // Update firstname of newly created record to 'Oskar'
            val updateQuery = "UPDATE student" +
                    "  SET student_firstname = 'Oskar'," +
                    "  student_lastname = 'Wallace'" +
                    "  WHERE student_id = 1;"
            val updateResult = session.execute(updateQuery)
        }

        CqlSession.builder().build().use { session ->
            val query = "SELECT * FROM student;"
            val rs = session.execute(query)
            val row = rs.all()
        }
    }
}

fun main(args: Array<String>) {
    // Perform queries
    val queries = CassandraQueries()
}